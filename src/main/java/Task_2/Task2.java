package Task_2;

import java.time.Year;
import java.*;
public class Task2 {
    public static void main(String[] args) {
        Command command;
//as lambda expression
        String s = "you";
        command = () -> s;
        System.out.println("I'm happy with " + command.beHappyString());
//as method reference


//        One one = new One();
//        one =() -> {
//            return command.beHappyString(command::setMe);
//        };



//
//
//
//        String s = command(Happy2 :: getS);
//
////as anonymous class
        command.beHappyString()new Command() {

            @Override
            public String beHappyString(String s) {
                return null;
            }
//
//            @Override
//            public String beHappyCommand(Command s) {
//                return null;
//            }
//        });
//
////as Object
//
//Command command1 = () -> System.out.println("I'm happy with ");
//    }
//
//
//}
//

    }
}
class One implements Command {


    String me = "him";

    public void setMe(String me) {
        this.me = me;
    }

    @Override
    public String beHappyString() {
        return null;
    }

}